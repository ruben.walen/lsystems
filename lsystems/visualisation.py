import lsystems as ls
import PILdraw as pildr
import random
import copy
import numpy as np
import turtle
import collections

def randomColorMap(symbols, deviation, seed):
    """Generate a random color map."""
    if deviation < 0 or deviation > 127:
        raise ValueError("@visualisation.randomColorMap: deviation must be between 0 and 127.""")
    rs = random.getstate()
    random.seed(seed)
    
    color_dict = {}
    BASE_COL = [127, 127, 127]
    for s in symbols:
        col = copy.copy(BASE_COL)
        for i in range(3):
            col[i] += int(random.random() * deviation)
        color_dict[s] = tuple(col)

    random.setstate(rs) # return state
    return color_dict

def drawLinesToGrid(filename, line_collector, color_map, mode="PNG", save=True, background_color=(255, 255, 255), max_size=(1000, 1000)):
    """Draw the given LineCollector of lines to a grid image (time dimension downwards)."""
    size_y = len(line_collector.lines) # size of t dim
    min_ind, max_ind = min(line_collector.indices), max(line_collector.indices)
    size_x = max([len(l) for l in line_collector.lines])
    if size_x > max_size[0]:
        raise ValueError("@visualisation.drawLinesToGrid: string size exceeds max_size bounds:" + str(size_x))
    if size_y > max_size[1]:
        raise ValueError("@visualisation.drawLinesToGrid: time dim size exceeds max_size bounds:" + str(size_y))
    value_dict = {} # relies on bound .lsystem
    color_dict = {} # numeric
    # value 0 for background and spacers
    for i, s in enumerate(line_collector.lsystem.symbols):
        value_dict[s] = i + 1
        color_dict[i + 1] = color_map[s]
    for s in line_collector.lsystem.spacers:
        value_dict[s] = 0
        color_dict[0] = background_color # should not be necessary

    array = np.full((size_x, size_y), 0)
    for i, l in enumerate(line_collector.lines):
        ind = line_collector.indices[i] - min_ind
        for j, s in enumerate(l):
            array[ind + j, i] = value_dict[s]

    img = pildr.arrayDraw(array, filename, color_dict, mode=mode, box_size=1, back_value=0,
                    back_color=background_color, save=save)
    return img
    
def drawTurtle(line_collector, distance=5, angle=60, instructions = None, create_instructions = True):
    """Draw a L-system using a Turtle. Instructions should be given in the form of a dictionary,
    with the L-system symbols being the keys and F, B, [, ], +, -, the values."""
    line = line_collector.lines[-1]
    stack = []
    t = turtle.Turtle()
    t.speed(0)
    if not instructions:
        if create_instructions:
            instructions = collections.defaultdict(list)
            for character in set(line):
                instructions[character] = random.choice(('F','B','+', '-', '[', ']'))
        else:
            instructions = collections.defaultdict({'F': 'F', 'B': 'B', '+': '+', '-': '-', '[': '[', ']': ']'})
    #instructions = {'B': turtle.forward, 'C': t.backward(distance), 'A': t.right(angle), 'D': t.left(angle)}
    for character in line:
        if instructions[character] == 'F':
            t.forward(distance)
        elif instructions[character] == 'B':
            t.backward(distance)
        elif instructions[character] == '+':
            t.right(angle)
        elif instructions[character] == '-':
            t.left(angle)
        elif instructions[character] == '[':
            stack.append(t.pos())
        elif instructions[character] == ']':
            if stack:
                t.setpos(stack.pop())
    turtle.exitonclick()
    turtle.bye()