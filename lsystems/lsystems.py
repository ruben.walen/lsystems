import math

def addSpacersToString(spacer, string, space_all=False):
    """This adds spacer characters (if possible) to a production string.
        Spacer characters are ignored in rule application.
        space_all (Boolean): adds spacers between all characters.
            if False: only adds spacer in the middle of even-numbered string"""
    #if spacer not in self.spacers:
    #    raise ValueError("@SimpleLSystem.addSpacersToString: spacer not in LSystem:" + spacer)
    if string == "": # return nothing
        return ""

    res_string = string # default
    L = len(string)
    if space_all == False:
        if L % 2 == 0: # even (case uneven --> default)
            index = int(L / 2)
            res_string = string[0:index] + spacer + string[index:]
    else:
        res_string = string[0]
        if L > 1:
            for s in string[1:]:
                res_string += spacer
                res_string += s
    return res_string

class SimpleLSystem:
    def __init__(self, symbols, prod_rules, spacers=["."]):
        """Initialize with symbols and production rules. "spacer" is a white-spacing non-evolving symbol."""
        self.symbols = symbols
        self.prod_rules = prod_rules # dictionary
        self.spacers = spacers # can be None
        self.has_spacer = True
        if spacers == None:
            self.has_spacer = False

    def addSymbol(self, symbol):
        if symbol not in self.symbols:
            self.symbols.append(symbol)

    def addRule(self, symbol_init, result_string):
        if symbol_init not in self.prod_rules.keys():
            self.prod_rules[symbol_init] = result_string
        else:
            print(">SimpleLSystem.addRule: WARNING: rule for " + symbol_init + " already exists; not overriding")

    def applyToString(self, string):
        """Apply the rules to a string."""
        res_string = ""
        for s in string: # any iterable
            if s not in self.spacers:
                try:
                    res_string += self.prod_rules[s]
                except KeyError:
                    raise ValueError("@SimpleLSystem.applyToString: symbol not found: " + s)
            else:
                res_string += s
        return res_string

class LineCollector:
    """Collects string lines from LSystem."""
    def __init__(self, lsystem, indexing_priority="left", axiom=None):
        self.lsystem = lsystem
        self.lines = []
        self.indices = [] # indices per line
        self.indexing_priority = indexing_priority
        if axiom != None:
            self.pushLine(axiom, self.computeIndex(axiom))

    def pushLine(self, line, index):
        self.lines.append(line)
        self.indices.append(index)

    def computeIndex(self, string):
        """Get sliding window index of leftmost string character."""
        L = len(string)
        if self.indexing_priority == "left":
            if L > 0:
                return -1
            else:
                return 0
        elif self.indexing_priority == "center-left":
            if L % 2 == 0:
                return -1 * int(L / 2)
            else:
                return -1 * (int(math.floor(L / 2)))
        elif self.indexing_priority == "center-right":
            if L % 2 == 0:
                return -1 * int(L / 2)
            else:
                return -1 * (int(math.floor(L / 2)) + 1)
        elif self.indexing_priority == "right":
            return -len(string)
        else:
            raise ValueError("@LineCollector.computeIndex: invalid indexing priority mode: " + indexing_priority)

    def applyToPreviousAndPush(self):
        """Applies lsystem to the previous line, and pushes the resulting line."""
        if len(self.lines) == 0:
            raise Exception("@LineCollector.applyToPreviousAndPush: self.lines is empty! Push a line first.")
        
        string = self.lsystem.applyToString(self.lines[-1])
        self.pushLine(string, self.computeIndex(string))

    def printTreeVisualisation(self):
        min_ind = min(self.indices)
        for i, l in enumerate(self.lines):
            diff = self.indices[i] - min_ind
            if diff > 0:
                print(" " * diff, end="")
            print(l, end="\n")

    def clear(self):
        self.lines = []
        self.indices = []

    def setAxiom(self, axiom):
        self.pushLine(axiom, self.computeIndex(axiom))
            

if __name__ == "__main__":
    sl = SimpleLSystem(["A", "B", "C"], {})
    sl.addRule("A", "AB")
    sl.addRule("B", "BAC")
    sl.addRule("C", "AD")
    sl.addRule("D", "A")

    curr_string = "A" # axiom
    for _ in range(10):
        print(curr_string)
        print(len(curr_string))
        curr_string = sl.applyToString(curr_string)


    sl2 = SimpleLSystem(["A", "E", "B", "C", "G", " "], {})
    sl2.addRule("A", "BE")
    sl2.addRule("E", "AG")
    sl2.addRule("B", "GAC ")
    sl2.addRule("C", "C")
    sl2.addRule("G", "B AG")
    sl2.addRule(" ", " ")

    curr_string = "A" # axiom
    for _ in range(10):
        print(curr_string)
        curr_string = sl2.applyToString(curr_string)

    sl3 = SimpleLSystem(["A", "(", ")", "B", "C", " "], {})
    sl3.addRule("A", "(B")
    sl3.addRule("B", "C)")
    sl3.addRule("(", "(")
    sl3.addRule(")", ")")
    sl3.addRule(" ", " ")
    sl3.addRule("C", "A B")
    
    lc = LineCollector(sl3, axiom="A", indexing_priority="center-left")
    for _ in range(12):
        lc.applyToPreviousAndPush()
    print(lc.lines)
    lc.printTreeVisualisation()
