import lsystems
import random

def generateSimpleSystem(symbols, terminal_indices, seed, self_repl_chance=0.5, len_target=2, len_deviation=2):
    """Attempt at generating simple rule system.
        Input: symbols (list(string)): contains all symbols.
                terminal_indices (list(int)): contain indices of terminal symbs.
                seed (int): random seed.
        Returns: tuple (symbols, prod_rules)"""
    rs = random.getstate()
    random.seed(seed)
    
    terminal_symbols = [symbols[i] for i in terminal_indices]
    non_terminal_symbols = [s for s in symbols if s not in terminal_symbols] # slow
    prod_rules = {s : "" for s in symbols}
    for s in terminal_symbols:
        prod_rules[s] = s

    for s in symbols:
        if s not in terminal_symbols: # slow
            drawable = [c for c in non_terminal_symbols if c != s]
            symbols_to_add = []
            if random.random() < self_repl_chance:
                symbols_to_add.append(s) # self-replication
            num_other_symbols = len_target + int(len_deviation * random.random())
            if num_other_symbols < 0:
                num_other_symbols = 0
            # can have more than len(symbols)
            for _ in range(num_other_symbols):
                symbols_to_add.append(random.choice(drawable))
            # cannot add self-repl here
            # can add same symbol multiple times
            # problem: unreachable symbols?

            # random permutation
            perm = random.sample(symbols_to_add, k=len(symbols_to_add))
            for c in perm:
                prod_rules[s] += c

    random.setstate(rs)
    return symbols, prod_rules
